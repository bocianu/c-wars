// const _ = require('lodash');

const thread = {
  pointer: null,
  lifespan: 0
};

const player = {
  name: null,
  lifespan: 0,
  step: 0,
  threads: []
};

const machineFactory = (memSize = 1024, handlers = {}) => {
  let cycle = 0;
  let memory = [];
  const memorySize = memSize || 1024;
  let players = [];

  const consoleWrite = msg => {
    if (handlers.console) {
      handlers.console(msg);
    } else {
      console.log(msg);
    }
  }

  const getMemory = () => memory;
  const getPlayers = () => players;

  const addPlayer = (name, threadPointer) => {
    const newPlayer = _.assign(_.cloneDeep(player), {
      name: name
    });
    const newThread = _.assign(_.cloneDeep(thread), {
      pointer: threadPointer
    });
    newPlayer.threads.push(newThread);
    players.push(newPlayer);
  }

  const addThread = (playerNum, threadPointer) => {
    const newThread = _.assign(_.cloneDeep(thread), {
      pointer: threadPointer
    });
    players[playerNum].threads.push(newThread);
  }

  const killThread = (playerNum, threadNum) => {
    consoleWrite('KILLTHREAD');
    players[playerNum].threads.splice(threadNum, 1);
  }


  const knownMnemonics = [
    'DAT', 'MOV', 'SPL', 'JMP', 'JMZ', 'JMN', 'SKZ', 'SKN',
    'INC', 'DEC', 'ADD', 'SUB', 'MUL', 'DIV', 'MOD'
  ];

  const addbits = s => {
      return (s.replace(/\s/g, '').match(/[+\-]?([0-9\.]+)/g) || [])
          .reduce(function(sum, value) {
              return parseFloat(sum) + parseFloat(value);
          });
  }

  const isNumeric = val => Number(parseFloat(val)) === val;

  const parseCodeArg = (arg, currPtr, labels) => {
    let argparsed = arg;
    let prefix='';
    if (_.includes(['#','@','^'], argparsed[0])) {
      prefix = argparsed[0];
      argparsed = argparsed.substr(1);
    }
    for (label of _.keys(labels)) {
      argparsed = _.replace(argparsed, label, Number(labels[label])-Number(currPtr));
    }
    // console.log(argparsed);
    if (!isNumeric(argparsed)) {
      argparsed = addbits(argparsed);
    }
    return `${prefix}${argparsed}`;
  }

  const parseCodeToArray = code => {
    let labels = {};
    let pointer = 0;
    let lines = _.split(code, '\n');
    // remove comments and empty lines
    lines = _.map(lines, line => _.split(line, ';')[0]);
    lines = _.filter(lines,line => _.trim(line)!='');
    // parse and remove labels first
    lines = _.map(lines, line => {
        const segments = _.map(_.split(_.trim(line), ' '), _.trim);
        let segIdx = 0;
        // label found
        if (_.endsWith(segments[0],':')) {
          labels[segments[0].slice(0,-1)] = pointer;
          segIdx++;
        };
        if (segments[segIdx]) {
          if (_.includes(knownMnemonics, segments[segIdx].toUpperCase())) {
            pointer++;
            return _.join(_.drop(segments, segIdx),' ');
          } else {
            consoleWrite(`unknown mnemonic: ${segments[segIdx]} - SKIPPED`);
          }
        } else {
          return '';
        }
    });
    // remove empty lines again
    lines = _.filter(lines,line => line!='');
    // and finaly parseArg
    pointer = 0;
    return _.map(lines, line => {
      const segments = _.split(line, ' ');
      segments[0] = segments[0].toUpperCase();
      if (segments[1]) { segments[1] = parseCodeArg(segments[1],pointer,labels) };
      if (segments[2]) { segments[2] = parseCodeArg(segments[2],pointer,labels) };
      pointer++;
      return _.join(segments, ' ');
    });
  }

  const loadPrograms = programs => {
    if (programs) {
      init();
      const segmentSize = Math.floor(memorySize / programs.length);
      _.each(programs, (program, programNum) => {
        consoleWrite(`*** Loading program ${programNum+1}...`);
        const memoryOffset = programNum * segmentSize;
        _.each(program, (cmd, idx) => memoryWrite(memoryOffset + idx, cmd, programNum));
        addPlayer(`Player${Number(programNum) + 1}`, memoryOffset);
      });
    };
  };

  const init = () => {
    consoleWrite('*** Machine initialized');
    cycle = 0;
    players = [];
    _.times(memorySize, i => memoryWrite(i,'DAT 0'));
  };

  const parseArg = arg => {
    if (!arg) {
      return null;
    }
    let addrMode = '';
    let value = _.parseInt(arg);
    if (_.isNaN(value)) {
      addrMode = arg[0];
      value = _.parseInt(arg.substr(1));
    }
    if (_.isNaN(value)) {
      throw `error parsing argument ${arg}`;
    }
    return {
      addrMode, value
    };
  }

  const parseCmd = cmd => {
    const cmdArray = cmd.split(' ');
    return {
      mnemonic: cmdArray[0].toUpperCase(),
      arg1: parseArg(cmdArray[1]),
      arg2: parseArg(cmdArray[2])
    }
  }

  const cmdToString = cmd => {
    let strcmd = cmd.mnemonic;
    if (cmd.arg1) {
      strcmd += ` ${cmd.arg1.addrMode}${cmd.arg1.value}`
    };
    if (cmd.arg2) {
      strcmd += ` ${cmd.arg2.addrMode}${cmd.arg2.value}`
    };
    return strcmd;
  }

  const lastArg = cmd =>  cmd.arg2 ? cmd.arg2 : cmd.arg1;
  const cmdToValue = cmd => lastArg(cmd).value;

  const fixAddress = addr => (addr + memorySize) % memorySize;

  const memoryWrite = (addr, val, playerNum) => {
    // consoleWrite(`######## memWrite: ${addr}:${val} `);
    if (handlers.memoryWrite) {handlers.memoryWrite(addr, val, playerNum)};
    memory[fixAddress(addr)] = val;
  }

  const updateMemValue = (addr, val, playerNum) => {
    const cmd = parseCmd(memory[addr]);
    if (cmd.arg2) {
      cmd.arg2.value = val;
    } else {
      cmd.arg1.value = val;
    }
    memoryWrite(addr, cmdToString(cmd), playerNum);
  }

  const readAddress = (arg, pointer) => {
    switch (arg.addrMode) {
      case '':
      case '#':
        return fixAddress(pointer + arg.value);
      case '^': // absolute
        return arg.value;
      case '@': // indirect
        const newPtr = fixAddress(pointer + arg.value);
        return readAddress(lastArg(parseCmd(memory[newPtr])), newPtr);
      default:
        throw `addressing mode error: ${arg.addrMode}`;
    }
  }

  const readValue = (arg, pointer) => {
    if (arg.addrMode === '#') {
      return arg.value;
    }
    return memory[readAddress(arg, pointer)];
  }

  const executeCmd = (cmd, cmdLocation, playerNum) => {
    const threadState = {
      result: 'jump',
      pointer: fixAddress(Number(cmdLocation) + 1)
    }

    //consoleWrite(`> Runing command: ${cmd}`);
    try {
      let addr, val;
      const order = parseCmd(cmd);
      switch (order.mnemonic) {

        case 'DAT':
          threadState.result = 'kill';
          break;

        case 'MOV':
          val = readValue(order.arg1, cmdLocation);
          addr = readAddress(order.arg2, cmdLocation);
          memoryWrite(addr, val, playerNum);
          break;

        case 'JMZ':
        case 'JMN':
          val = cmdToValue(readValue(order.arg1, cmdLocation));
          addr = readAddress(order.arg2, cmdLocation);
          if ((val === 0 && order.mnemonic === 'JMZ') ||
            (val !== 0 && order.mnemonic === 'JMN')) {
            threadState.pointer = addr;
          }
          break;

        case 'SKZ':
        case 'SKN':
          val = cmdToValue(readValue(order.arg1, cmdLocation));
          addr = readAddress(order.arg2, cmdLocation);
          if ((val === 0 && order.mnemonic === 'SZE') ||
            (val !== 0 && order.mnemonic === 'SNZ')) {
            threadState.pointer = fixAddress(Number(cmdLocation) + 2)
          }
          break;

        case 'JMP':
          addr = readAddress(order.arg1, cmdLocation);
          threadState.pointer = addr;
          break;

        case 'SPL':
          addr = readAddress(order.arg1, cmdLocation);
          threadState.splitPointer = addr;
          threadState.result = 'split';
          break;

        case 'INC':
        case 'DEC':
          addr = readAddress(order.arg1, cmdLocation);
          val = cmdToValue(parseCmd(memory[addr]));
          if (order.mnemonic === 'INC') { val++; } else { val--; };
          updateMemValue(addr, val, playerNum);
          break;

        case 'ADD':
        case 'SUB':
        case 'MUL':
        case 'DIV':
        case 'MOD':
          const operand = readValue(order.arg1, cmdLocation);
          addr = readAddress(order.arg2, cmdLocation);
          val = cmdToValue(parseCmd(memory[addr]));
          switch (order.mnemonic) {
            case 'ADD': val += operand; break;
            case 'SUB': val -= operand; break;
            case 'MUL': val *= operand; break;
            case 'DIV': val = Math.floor(val/operand); break;
            case 'MOD': val = val % operand; break;
          }
          updateMemValue(addr, val, playerNum);
          break;

        default:
          throw `Unknown mnemonic: ${order.mnemonic}`;
      }

    } catch (e) {
      consoleWrite(`!!! Error during command execution: ${cmd}\n${e}`);
      consoleWrite(`!!! Thread killed`);
      threadState.result = 'kill';
    }
    return threadState;
  }

  const stepForward = (cycles = 1) => {
    while (cycles > 0) {
      _.each(players, (player, playerNum) => {
        const currentThread = player.threads[player.step];
        if (currentThread) {
          const command = memory[currentThread.pointer];
          const threadState = executeCmd(command, currentThread.pointer, playerNum);
          switch (threadState.result) {
            case 'kill':
              killThread(playerNum, player.step);
              break;
            case 'split':
              addThread(playerNum, threadState.splitPointer);
            default:
              currentThread.pointer = threadState.pointer;
          }
          player.step = (player.step + 1) % player.threads.length;
        }
      })
      cycle++;
      cycles--;
    }
  };

  const state = () => {
    consoleWrite('*** Machine State:')
    //consoleWrite(JSON.stringify(memory.slice(0, 16)));
    _.each(players, p => {
      consoleWrite(`${p.name} threads: ${p.threads.length}`)
    });
    consoleWrite(`cycle: ${cycle}`);
    //return cycle;
  }

  init();

  return {
    init, stepForward, state, getPlayers, parseCodeToArray, loadPrograms
  }
}
