const gui = {
  delay: 100,
  machineStarted: false,
  cycleInterval: 0,
  machine: {},
  console: msg => {
    $('#console').append(msg+'\n').scrollTop($('#console')[0].scrollHeight);
  },

  initMemory: size => {
    $('#memory').empty();
    const width = Math.floor(Math.sqrt(size));
    for (let i = 0; i < size; i++) {
      $('<div/>',{
        id: `cell_${i}`,
        class: 'memory_cell ' + (i%width==0?'break':'')
      }).appendTo('#memory');
    }
  },

  memoryWrite: (addr, val, player) => {
    $(`#cell_${addr}`).removeClass('written_by_player_0 written_by_player_1').addClass(`written_by_player_${player}`).attr('title', val);
  },

  updatePointers: (players) => {
    $('.memory_cell').removeClass('pointer_player_0 pointer_player_1');
    _.each(players, (player, playerNum) => {
      _.each(player.threads, t => {
        $(`#cell_${t.pointer}`).addClass(`pointer_player_${playerNum}`)
      })
    });
  },

  loadPrograms: () => {
    if (gui.machineStarted) {
      clearInterval(gui.cycleInterval);
      gui.machineStarted = false;
      $('#start').removeClass('started');
    };
    const pgm1 = gui.machine.parseCodeToArray($('#code1').val());
    const pgm2 = gui.machine.parseCodeToArray($('#code2').val());
    gui.machine.loadPrograms([pgm1, pgm2]);
    const players = gui.machine.getPlayers();
    gui.updatePointers(players);

  },

  cycle: () => {
    gui.machine.stepForward();
    const players = gui.machine.getPlayers();
    gui.updatePointers(players);
  },

  startStop: () => {
    if (gui.machineStarted) {
      clearInterval(gui.cycleInterval);
      gui.machineStarted = false;
      $('#start').removeClass('started');
    } else {
      gui.cycleInterval = setInterval(gui.cycle, gui.delay);
      gui.machineStarted = true;
      $('#start').addClass('started');
    }
  },

  speedChange: (p) => {
      gui.delay =$('#speed_slider')[0].value;
      $('#speed_value').html(gui.delay);
      if (gui.machineStarted) {
          clearInterval(gui.cycleInterval);
          gui.cycleInterval = setInterval(gui.cycle, gui.delay);
      }
  },

  init: (memSize, machine) => {
    gui.machine = machine;
    gui.initMemory(memSize);
    $('<button/>', {class: 'machine_button', id: 'start'}).html('START / STOP').bind('click', gui.startStop).appendTo('#menu');
    $('<button/>', {class: 'machine_button'}).html('STEP').bind('click', gui.cycle).appendTo('#menu');
    $('<button/>', {class: 'machine_button'}).html('STATUS').bind('click', gui.machine.state).appendTo('#menu');
    $('#speed_slider').bind('change', gui.speedChange);
    $('<button/>', {class: 'machine_button br'}).html('LOAD PROGRAMS').bind('click', gui.loadPrograms).appendTo('#code_editors');
    // allow tab in texareas
    $(document).delegate('.code_area', 'keydown', function(e) {
        var keyCode = e.keyCode || e.which;

        if (keyCode == 9) {
          e.preventDefault();
          var start = this.selectionStart;
          var end = this.selectionEnd;

          // set textarea value to: text before caret + tab + text after caret
          $(this).val($(this).val().substring(0, start)
                      + "\t"
                      + $(this).val().substring(end));

          // put caret at right position again
          this.selectionStart =
          this.selectionEnd = start + 1;
        }
    });
  }
};

$(document).ready(function() {
  const memSize = 1024;
  gui.init(memSize, machineFactory(memSize, {
      console: gui.console,
      memoryWrite: gui.memoryWrite,
  }));
  gui.machine.state();
  gui.updatePointers(gui.machine.getPlayers());
});
